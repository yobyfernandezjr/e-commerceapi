const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Full Name is required"]
	},
	email: {
		type: String,
		required: [true, 'Email address is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	reservation: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			reservedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Reserved"
			}
		}
	]
});

module.exports = mongoose.model('User', userSchema) || mongoose.models.User;